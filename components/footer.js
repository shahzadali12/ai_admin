import React from "react";

function Footer(props) {
  return (
    <footer className="footer text-center">
      © seemyAi{" "}
      <span className="d-none d-sm-inline-block">
        {" "}
        - Crafted with <i className="mdi mdi-heart text-danger"></i> by
        Shahzad-Ali.
      </span>
    </footer>
  );
}

export default Footer;
