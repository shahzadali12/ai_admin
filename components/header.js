import React from 'react';

function Header(props) {
    return (
        <header>
        <div className="d-flex">
          <div className="navbar-brand-box text-center flex justify-center items-center">
            <a href="index.html" className="logo logo-light">
              <span className="logo-sm">
                <img src="/images/logo-sm.png" className='w-3/4' alt="" />
              </span>
              <span className="logo-lg">
                <img src="/images/logo.png" alt="" className='w-3/4' />
              </span>
            </a>
            {/* <a href="index.html" className="logo logo-dark">
              <span className="logo-sm">
                <img src="/images/logo-sm.png" alt="" height="22" />
              </span>
              <span className="logo-lg">
                <img src="/images/logo_dark.png" alt="" height="20" />
              </span>
            </a> */}
          </div>

          <div className="navbar-header">
            <button
              type="button"
              className="button-menu-mobile waves-effect"
              id="vertical-menu-btn"
            >
              <i className="mdi mdi-menu"></i>
            </button>
            <div className="d-flex ms-auto">
              <div className="search-wrap" id="search-wrap">
                <div className="search-bar">
                  <input
                    className="search-input form-control"
                    placeholder="Search"
                  />
                  <a
                    href="#"
                    className="close-search toggle-search"
                    data-target="#search-wrap"
                  >
                    <i className="mdi mdi-close-circle"></i>
                  </a>
                </div>
              </div>

              {/* <div className="dropdown d-none d-md-block">
                <button
                  type="button"
                  className="btn header-item waves-effect"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    className="flag-img me-2"
                    src="/images/flags/us_flag.jpg"
                    alt="Header Language"
                    height="16"
                  />{" "}
                  English <i className="mdi mdi-chevron-down"></i>
                </button>
                <div className="dropdown-menu dropdown-menu-end">
                  <a
                    href=""
                    className="dropdown-item notify-item"
                  >
                    <img
                      src="/images/flags/germany_flag.jpg"
                      alt="user-image"
                      height="12"
                    />{" "}
                    <span className="align-middle"> German </span>
                  </a>

                  <a
                    href=""
                    className="dropdown-item notify-item"
                  >
                    <img
                      src="/images/flags/italy_flag.jpg"
                      alt="user-image"
                      height="12"
                    />{" "}
                    <span className="align-middle"> Italian </span>
                  </a>

                  <a
                    href=""
                    className="dropdown-item notify-item"
                  >
                    <img
                      src="/images/flags/french_flag.jpg"
                      alt="user-image"
                      height="12"
                    />{" "}
                    <span className="align-middle"> French </span>
                  </a>

                  <a
                    href=""
                    className="dropdown-item notify-item"
                  >
                    <img
                      src="/images/flags/spain_flag.jpg"
                      alt="user-image"
                      height="12"
                    />{" "}
                    <span className="align-middle"> Spanish </span>
                  </a>

                  <a
                    href=""
                    className="dropdown-item notify-item"
                  >
                    <img
                      src="/images/flags/russia_flag.jpg"
                      alt="user-image"
                      height="12"
                    />{" "}
                    <span className="align-middle"> Russian </span>
                  </a>
                </div>
              </div> */}

              {/* <div className="dropdown">
                <button
                  type="button"
                  className="btn header-item toggle-search noti-icon waves-effect"
                  data-target="#search-wrap"
                >
                  <i className="mdi mdi-magnify"></i>
                </button>
              </div>

              <div className="dropdown d-inline-block">
                <button
                  type="button"
                  className="btn header-item noti-icon waves-effect"
                  id="page-header-notifications-dropdown"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="mdi mdi-bell-outline"></i>
                  <span className="badge bg-danger rounded-pill">3</span>
                </button>
                <div
                  className="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                  aria-labelledby="page-header-notifications-dropdown"
                >
                  <div className="p-3">
                    <div className="row align-items-center">
                      <div className="col">
                        <h5 className="m-0 font-size-16"> Notification (3) </h5>
                      </div>
                    </div>
                  </div>

                  <div data-simplebar >
                    <a
                      href="#"
                      className="text-reset notification-item d-block active"
                    >
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-success rounded-circle font-size-16">
                            <i className="mdi mdi-cart-outline"></i>
                          </span>
                        </div>
                        <div className="flex-1">
                          <h6 className="mt-0 font-size-15 mb-1">
                            Your order is placed
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1">
                              Dummy text of the printing and typesetting
                              industry.
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>

                    <a href="#" className="text-reset notification-item d-block">
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-danger rounded-circle font-size-16">
                            <i className="mdi mdi-message-text-outline"></i>
                          </span>
                        </div>
                        <div className="flex-1">
                          <h6 className="mt-0 font-size-15 mb-1">
                            New Message received
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1">You have 87 unread messages</p>
                          </div>
                        </div>
                      </div>
                    </a>

                    <a href="#" className="text-reset notification-item d-block">
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-info rounded-circle font-size-16">
                            <i className="mdi mdi-glass-cocktail"></i>
                          </span>
                        </div>
                        <div className="flex-1">
                          <h6 className="mt-0 font-size-15 mb-1">
                            Your item is shipped
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1">
                              It is a long established fact that a reader will
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a href="#" className="text-reset notification-item d-block">
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-success rounded-circle font-size-16">
                            <i className="mdi mdi-message-text-outline"></i>
                          </span>
                        </div>
                        <div className="flex-1">
                          <h6 className="mt-0 font-size-15 mb-1">
                            New Message received
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1">You have 87 unread messages</p>
                          </div>
                        </div>
                      </div>
                    </a>

                    <a href="#" className="text-reset notification-item d-block">
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-warning rounded-circle font-size-16">
                            <i className="mdi mdi-cart-outline"></i>
                          </span>
                        </div>
                        <div className="flex-1">
                          <h6 className="mt-0 font-size-15 mb-1">
                            Your order is placed
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1">
                              Dummy text of the printing and typesetting
                              industry.
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>

                  <div className="p-2 border-top">
                    <div className="d-grid">
                      <a
                        className="btn btn-sm btn-link font-size-14 text-start"
                        href="javascript:void(0)"
                      >
                        View all
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="dropdown d-inline-block">
                <button
                  type="button"
                  className="btn header-item waves-effect"
                  id="page-header-user-dropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    className="rounded-circle header-profile-user me-2"
                    src="/images/users/avatar-1.jpg"
                    alt="Header Avatar"
                  />
                  <span className="d-none d-md-inline-block ms-1">
                    Donald T. <i className="mdi mdi-chevron-down"></i>{" "}
                  </span>
                </button>
                <div className="dropdown-menu dropdown-menu-end">
                  <a className="dropdown-item" href="#">
                    <i className="dripicons-user font-size-16 align-middle d-inline-block me-1"></i>{" "}
                    Profile
                  </a>
                  <a className="dropdown-item" href="#">
                    <i className="dripicons-wallet font-size-16 align-middle d-inline-block me-1"></i>{" "}
                    My Wallet
                  </a>
                  <a className="dropdown-item d-block" href="#">
                    <span className="badge bg-success float-end">11</span>
                    <i className="dripicons-gear font-size-16 align-middle me-1"></i>{" "}
                    Settings
                  </a>
                  <a className="dropdown-item" href="#">
                    <i className="dripicons-lock-open font-size-16 align-middle d-inline-block me-1"></i>{" "}
                    Lock screen
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item text-danger" href="#">
                    <i className="dripicons-power-off font-size-16 align-middle me-1 text-danger"></i>{" "}
                    Logout
                  </a>
                </div>
              </div>

              <div className="dropdown d-inline-block">
                <button
                  type="button"
                  className="btn header-item noti-icon right-bar-toggle waves-effect"
                >
                  <i className="mdi mdi-spin mdi-cog"></i>
                </button>
              </div> */}
            </div>
          </div>
        </div>
      </header>
    );
}

export default Header;