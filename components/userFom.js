import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemas } from "./schema/userFom";
import axios from "axios";

function EditUser({ setshowOverlay, Rowdata, setRefresh }) {
  

  const [loader, setLoader] = useState(false)
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    watch,
    clearErrors,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const values = getValues();
  const watchs = watch()

  const onSubmit = async (data) => {
    setLoader(true)
    const SendingDAta = {
      _id:Rowdata._id ? Rowdata._id : "",
      name:data.name,
      email: Rowdata.email ? Rowdata.email : data.email,
      firstName: Rowdata.firstName? Rowdata.firstName : data.firstName,
      lastName: Rowdata.lastName? Rowdata.lastName : data.lastName,
      profilePhoto: Rowdata.profilePhoto? Rowdata.profilePhoto : data.profilePhoto,
      password: Rowdata.password? Rowdata.password : data.password,
      salt: Rowdata.salt? Rowdata.salt : data.salt,
      role: data.role,
    }

   
    
    const header_options = {
      method: Rowdata?._id ? "PUT" : "POST",
      mode: "cors",
      url: `${process.env.REACT_APP_BaseURL}/api/authenticated/create`,
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data: SendingDAta,
    };

    await axios.request(header_options).then(function (responce) {
      if (responce?.status === 201) {
        if (Rowdata.icon) {
          toast.success("User Updated sucessfully!")
          setLoader(false)
        } else {
          toast.success("User created sucessfully!")
          setLoader(false)
        }
        setRefresh(responce)
        setshowOverlay(false)
        reset({});
        setLoader(false)

      }
    }).catch(function (error) {
      toast.error(error?.response?.data);
      setLoader(false)
    });
  };



  useEffect(() => {

    if (Rowdata && Rowdata) {
      const { name, email,role,_id,hashed_password } = Rowdata;

      setValue("name", name)
      setValue("email", email)
      setValue("role",role)
      setValue("password",hashed_password)
      setValue("_id",_id)
    }
  }, [Rowdata?._id])



  return (
    <>
      <div className="py-0.5">
        <div className="container">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
              <div className="col-md-12">
                <h4>User information</h4>
                <p>Please fill the all compulsory fields</p>
                <hr className="my-4 " />
              </div>

              <div className="col-md-12">
                <div className="row">
                  <div className={`col-md-6 relative text_felid ${errors.name ? "error" : ""}`}>
                    <label>
                      {errors.name ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.name?.message}
                        </>
                      ) : (
                        <>
                          name
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        {...register("name", { required: true })}
                        placeholder="name"
                      />
                    </div>
                  </div>

                  <div className={`col-md-6 relative text_felid ${errors.email ? "error" : ""}`}>
                    <label>
                      {errors.email ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.email?.message}
                        </>
                      ) : (
                        <>
                          Email
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        // disabled={true}
                        {...register("email", { required: true })}
                        placeholder="email"
                      />
                    </div>
                  </div>
                  <div className={`col-md-6 relative text_felid ${errors.password ? "error" : ""}`}>
                    <label>
                      {errors.password ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.password?.message}
                        </>
                      ) : (
                        <>
                          password
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        
                        {...register("password", { required: true })}
                        placeholder="password"
                      />
                    </div>
                  </div>
                  <div className={`col-md-6 relative text_felid ${errors.role ? "error" : ""}`}>
                    <label>
                      {errors.role ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.role?.message}
                        </>
                      ) : (
                        <>
                          role
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        disabled={Rowdata?.role ?  true : false} 
                        {...register("role", { required: true })}
                        placeholder="role"
                      />
                    </div>
                  </div>
                  {/* <div className={`col-md-12 text_felid ${errors.role ? "error" : ""}`}>
                    <label className="mt-1">
                      {errors.role ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.role?.message}
                        </>
                      ) : (
                        <>
                          role
                        </>
                      )}
                    </label>
                    <div className="position-relative text_felid">
                      <Editor setValue={setValue} name={'role'} state={values?.role} error={errors?.role} clearErrors={clearErrors} />
                    </div>
                  </div> */}

                </div>

              </div>

              <div className="col-md-12 mb-4">
                <button type="submit" disabled={loader ? true : false} className="bg-blue-500  px-5 py-2 border-0 rounded-md text-white">{loader ? <><i className="fa fa-spinner fa-spin fa-2x fa-fw"></i></> : Rowdata?._id ? "Update" : "Submit"}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
export default EditUser;
