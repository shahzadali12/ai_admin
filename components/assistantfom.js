import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemas } from "./schema/assistantform";
import axios from "axios";
import Editor from "./editor";

function EditFom({ setshowOverlay, Rowdata, setRefresh }) {
  

  const [loader, setLoader] = useState(false)
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    watch,
    clearErrors,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const values = getValues();
  const watchs = watch()


 
  function RemoveTags(tags) {
    var htmlRegexG = /<(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>/g;
    return tags.replaceAll(htmlRegexG, "");
  }
  const onSubmit = async (data) => {
    setLoader(true)
    const SendingDAta = {
      _id:Rowdata._id ? Rowdata._id : "",
      name:data.name,
      assisid: Rowdata.assisid ? Rowdata.assisid : "",
      instructions: RemoveTags(data.instructions),
      question:"hi"
    }
    
    const header_options = {
      method: Rowdata?._id ? "PUT" : "POST",
      mode: "cors",
      url: `${process.env.REACT_APP_BaseURL}/api/assistant/assistant`,
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data: SendingDAta,
    };

    await axios.request(header_options).then(function (responce) {
      if (responce?.status === 200) {
        if (Rowdata.icon) {
          toast.success("Assistant Updated sucessfully!")
          setLoader(false)
        } else {
          toast.success("Assistant created sucessfully!")
          setLoader(false)
        }
        setRefresh(responce)
        setshowOverlay(false)
        reset({});
        setLoader(false)

      }
    }).catch(function (error) {
      toast.error(error?.response?.data);
      setLoader(false)
    });
  };



  useEffect(() => {

    if (Rowdata && Rowdata) {
      const { name, assisid,instructions,_id } = Rowdata;

      setValue("name", name)
      setValue("assisid", assisid)
      setValue("instructions",instructions)
      setValue("_id",_id)
    }
  }, [Rowdata?._id])



  return (
    <>
      <div className="py-0.5">
        <div className="container">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
              <div className="col-md-12">
                <h4>Assistant information</h4>
                <p>Please fill the all compulsory fields</p>
                <hr className="my-4 " />
              </div>

              <div className="col-md-12">
                <div className="row">
                  <div className={`col-md-6 relative text_felid ${errors.name ? "error" : ""}`}>
                    <label>
                      {errors.name ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.name?.message}
                        </>
                      ) : (
                        <>
                          name
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        {...register("name", { required: true })}
                        placeholder="name"
                      />
                    </div>
                  </div>

                  <div className={`col-md-6 relative text_felid ${errors.assisid ? "error" : ""}`}>
                    <label>
                      {errors.assisid ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.assisid?.message}
                        </>
                      ) : (
                        <>
                          Assistant id
                          {/* <span className="text-red-500">*</span> */}
                        </>
                      )}
                    </label>
                    <div className="relative text_felid">
                      <input
                        type="text"
                        disabled={true}
                        {...register("assisid", { required: true })}
                        placeholder="Assistant id"
                      />
                    </div>
                  </div>
                  <div className={`col-md-12 text_felid ${errors.instructions ? "error" : ""}`}>
                    <label className="mt-1">
                      {errors.instructions ? (
                        <>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          ></i>
                          {errors?.instructions?.message}
                        </>
                      ) : (
                        <>
                          instructions
                        </>
                      )}
                    </label>
                    <div className="position-relative text_felid">
                      <Editor setValue={setValue} name={'instructions'} state={values?.instructions} error={errors?.instructions} clearErrors={clearErrors} />
                    </div>
                  </div>

                </div>

              </div>

              <div className="col-md-12 mb-4">
                <button type="submit" disabled={loader ? true : false} className="bg-blue-500  px-5 py-2 border-0 rounded-md text-white">{loader ? <><i className="fa fa-spinner fa-spin fa-2x fa-fw"></i></> : Rowdata?._id ? "Update" : "Submit"}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
export default EditFom;
