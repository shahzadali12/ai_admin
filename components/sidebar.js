import React from "react";
import Link from "next/link";
function SideBar(props) {
  return (
    <div className="vertical-menu">
      <div data-simplebar className="h-100">
        <div id="sidebar-menu">
          <ul className="metismenu list-unstyled" id="side-menu">
            <li className="menu-title text-uppercase">Main</li>

            <li>
              <Link href="/" className="waves-effect">
                <span className="badge rounded-pill bg-info float-end">2</span>
                <i className="dripicons-meter"></i>
                <span> Dashboard </span>
              </Link>
            </li>

            {/* <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-message"></i>
                <span> Email </span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="email-inbox.html">Inbox</a>
                </li>
                <li>
                  <a href="email-read.html">Email Read</a>
                </li>
                <li>
                  <a href="email-compose.html">Email Compose</a>
                </li>
              </ul>
            </li> */}
             <li>
              <Link href="/assistants" className="waves-effect">
                <i className="dripicons-calendar"></i>
                <span>Assistants</span>
              </Link>
            </li>
            <li>
              <Link href="/charactors" className="waves-effect">
                <i className="dripicons-calendar"></i>
                <span>Charactors</span>
              </Link>
            </li>
            <li>
              <Link href="/users" className="waves-effect">
                <i className="dripicons-calendar"></i>
                <span>users</span>
              </Link>
            </li>
            
            {/*

            <li className="">
              <a
                href="javascript: void(0);"
                className="has-arrow waves-effect"
                aria-expanded="true"
              >
                <i className="dripicons-monitor"></i>
                <span>Layouts</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="javascript: void(0);" className="has-arrow">
                    Vertical
                  </a>
                  <ul className="sub-menu mm-collapse" aria-expanded="true">
                    <li>
                      <a href="layouts-light-sidebar.html">Light Sidebar</a>
                    </li>
                    <li>
                      <a href="layouts-compact-sidebar.html">Compact Sidebar</a>
                    </li>
                    <li>
                      <a href="layouts-icon-sidebar.html">Icon Sidebar</a>
                    </li>
                    <li>
                      <a href="layouts-boxed.html">Boxed Layout</a>
                    </li>
                    <li>
                      <a href="layouts-dark-sidebar.html">Dark Sidebar</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="javascript: void(0);" className="has-arrow">
                    Horizontal
                  </a>
                  <ul className="sub-menu mm-collapse" aria-expanded="false">
                    <li>
                      <a href="layouts-horizontal.html">Horizontal</a>
                    </li>
                    <li>
                      <a href="layouts-hori-full.html">Full Width</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>

            <li className="menu-title">Components</li>

            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-briefcase"></i>
                <span>UI Elements </span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="ui-alerts.html">Alerts</a>
                </li>
                <li>
                  <a href="ui-badge.html">Badge</a>
                </li>
                <li>
                  <a href="ui-buttons.html">Buttons</a>
                </li>
                <li>
                  <a href="ui-cards.html">Cards</a>
                </li>
                <li>
                  <a href="ui-dropdowns.html">Dropdowns</a>
                </li>
                <li>
                  <a href="ui-navs.html">Navs</a>
                </li>
                <li>
                  <a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a>
                </li>
                <li>
                  <a href="ui-modals.html">Modals</a>
                </li>
                <li>
                  <a href="ui-images.html">Images</a>
                </li>
                <li>
                  <a href="ui-progressbars.html">Progress Bars</a>
                </li>
                <li>
                  <a href="ui-lightbox.html">Lightbox</a>
                </li>
                <li>
                  <a href="ui-pagination.html">Pagination</a>
                </li>
                <li>
                  <a href="ui-popover-tooltips.html">Popover & Tooltips</a>
                </li>
                <li>
                  <a href="ui-carousel.html">Carousel</a>
                </li>
                <li>
                  <a href="ui-video.html">Video</a>
                </li>
                <li>
                  <a href="ui-typography.html">Typography</a>
                </li>
                <li>
                  <a href="ui-sweet-alert.html">Sweet-Alert</a>
                </li>
                <li>
                  <a href="ui-grid.html">Grid</a>
                </li>
                <li>
                  <a href="ui-utilities.html">Utilities</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-broadcast"></i>
                <span>Icons</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="icons-material.html">Material Design</a>
                </li>
                <li>
                  <a href="icons-ion.html">Ion Icons</a>
                </li>
                <li>
                  <a href="icons-fontawesome.html">Font Awesome</a>
                </li>
                <li>
                  <a href="icons-themify.html">Themify Icons</a>
                </li>
                <li>
                  <a href="icons-dripicons.html">Dripicons</a>
                </li>
                <li>
                  <a href="icons-typicons.html">Typicons Icons</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="waves-effect">
                <span className="badge rounded-pill bg-success float-end">
                  8
                </span>
                <i className="dripicons-to-do"></i>
                <span>Forms</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="form-elements.html">Form Elements</a>
                </li>
                <li>
                  <a href="form-validation.html">Form Validation</a>
                </li>
                <li>
                  <a href="form-advanced.html">Form Advanced</a>
                </li>
                <li>
                  <a href="form-editors.html">Form Editors</a>
                </li>
                <li>
                  <a href="form-uploads.html">Form File Upload</a>
                </li>
                <li>
                  <a href="form-mask.html">Form Mask</a>
                </li>
                <li>
                  <a href="form-xeditable.html">Form Xeditable</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-graph-bar"></i>
                <span>Charts</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="charts-morris.html">Morris Charts</a>
                </li>
                <li>
                  <a href="charts-chartist.html">Chartist Charts</a>
                </li>
                <li>
                  <a href="charts-chartjs.html">Chartjs Charts</a>
                </li>
                <li>
                  <a href="charts-flot.html">Flot Charts</a>
                </li>
                <li>
                  <a href="charts-c3.html">C3 Charts</a>
                </li>
                <li>
                  <a href="charts-other.html">Jquery Knob Charts</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-view-thumb"></i>
                <span>Tables</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="tables-basic.html">Basic Tables</a>
                </li>
                <li>
                  <a href="tables-datatable.html">Data Tables</a>
                </li>
                <li>
                  <a href="tables-responsive.html">Responsive Table</a>
                </li>
                <li>
                  <a href="tables-editable.html">Editable Table</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="waves-effect">
                <span className="badge rounded-pill bg-danger float-end">
                  2
                </span>
                <i className="dripicons-map"></i>
                <span>Maps</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="maps-google.html">Google Maps</a>
                </li>
                <li>
                  <a href="maps-vector.html">Vector Maps</a>
                </li>
              </ul>
            </li>

            <li className="menu-title">Extras</li>
            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-archive"></i>
                <span>Advanced UI</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="advanced-animation.html">Animation</a>
                </li>
                <li>
                  <a href="advanced-highlight.html">Highlight</a>
                </li>
                <li>
                  <a href="advanced-rating.html">Rating</a>
                </li>
                <li>
                  <a href="advanced-nestable.html">Nestable</a>
                </li>
                <li>
                  <a href="advanced-alertify.html">Alertify</a>
                </li>
                <li>
                  <a href="advanced-rangeslider.html">Range Slider</a>
                </li>
                <li>
                  <a href="advanced-sessiontimeout.html">Session Timeout</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-box"></i>
                <span>Authentication</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="pages-login.html">Login</a>
                </li>
                <li>
                  <a href="pages-register.html">Register</a>
                </li>
                <li>
                  <a href="pages-recoverpw.html">Recover Password</a>
                </li>
                <li>
                  <a href="pages-lock-screen.html">Lock Screen</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-duplicate"></i>
                <span>Extra Pages</span>
              </a>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <a href="pages-timeline.html">Timeline</a>
                </li>
                <li>
                  <a href="pages-invoice.html">Invoice</a>
                </li>
                <li>
                  <a href="pages-directory.html">Directory</a>
                </li>
                <li>
                  <a href="pages-starter.html">Starter Page</a>
                </li>
                <li>
                  <a href="pages-404.html">Error 404</a>
                </li>
                <li>
                  <a href="pages-500.html">Error 500</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="javascript: void(0);" className="has-arrow waves-effect">
                <i className="dripicons-network-1"></i>
                <span>Multi Level</span>
              </a>
              <ul className="sub-menu" aria-expanded="true">
                <li>
                  <a href="javascript: void(0);">Level 1.1</a>
                </li>
                <li>
                  <a href="javascript: void(0);" className="has-arrow">
                    Level 1.2
                  </a>
                  <ul className="sub-menu" aria-expanded="true">
                    <li>
                      <a href="javascript: void(0);">Level 2.1</a>
                    </li>
                    <li>
                      <a href="javascript: void(0);">Level 2.2</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li> */}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default SideBar;
