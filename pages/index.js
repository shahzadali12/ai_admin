import Image from "next/image";

function Home() {
  return (
    <>
      <div className="page-content">
        <div className="container-fluid">
          <div className="page-title-box">
            <div className="row align-items-center">
              <div className="col-md-8">
                <h6 className="page-title">Dashboard</h6>
                <ol className="breadcrumb m-0">
                  <li className="breadcrumb-item">
                    <a href="#">seemyAi</a>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    Dashboard
                  </li>
                </ol>
              </div>
              {/* <div className="col-md-4">
                <div className="float-end d-none d-md-block">
                  <div className="dropdown">
                    <button
                      className="btn btn-primary btn-rounded dropdown-toggle"
                      type="button"
                      id="dropdownMenuButton"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      <i className="ti-settings me-1"></i> Settings{" "}
                      <i className="mdi mdi-chevron-down"></i>
                    </button>
                    <div className="dropdown-menu dropdown-menu-end">
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                      <a className="dropdown-item" href="#">
                        Another action
                      </a>
                      <a className="dropdown-item" href="#">
                        Something else here
                      </a>
                      <div className="dropdown-divider"></div>
                      <a className="dropdown-item" href="#">
                        Separated link
                      </a>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>

          <div className="row">
            <div className="col-xl-3 col-md-6">
              <div className="card mini-stats">
                <div className="p-3 mini-stats-content">
                  <div className="mb-4 ">
                    <div className="float-end text-end">
                      <span className="badge bg-light text-info my-2">
                        {" "}
                        + 11%{" "}
                      </span>
                      <p className="text-white-50">ChatGPT Assistants</p>
                    </div>
                    <span
                      className="peity-pie"
                      data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}'
                      data-width="54"
                      data-height="54"
                    >
                      5/8
                    </span>
                  </div>
                </div>
                <div className="mx-3">
                  <div className="card mb-0 border px-3 py-1 pt-3 mini-stats-desc">
                    <div className="d-flex">
                      <h6 className="mt-0 mb-0 leading-none">Assistants</h6>
                      <h5 className="ms-auto mt-0 leading-none">1758</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="card mini-stats">
                <div className="p-3 mini-stats-content">
                  <div className="mb-4">
                    <div className="float-end text-end">
                      <span className="badge bg-light text-danger my-2">
                        {" "}
                        - 27%{" "}
                      </span>
                      <p className="text-white-50">Sitepal Charactors</p>
                    </div>
                    <span
                      className="peity-donut"
                      data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"], "innerRadius": 18, "radius": 32 }'
                      data-width="54"
                      data-height="54"
                    >
                      2/5
                    </span>
                  </div>
                </div>
                <div className="mx-3">
                  <div className="card mb-0 border px-3 py-1 pt-3 mini-stats-desc">
                    <div className="d-flex">
                      <h6 className="mt-0 mb-0 leading-none">Charactors</h6>
                      <h5 className="ms-auto mt-0 leading-none">48259</h5>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="card mini-stats">
                <div className="p-3 mini-stats-content">
                  <div className="mb-4">
                    <div className="float-end text-end">
                      <span className="badge bg-light text-primary my-2">
                        {" "}
                        0%{" "}
                      </span>
                      <p className="text-white-50">Users</p>
                    </div>
                    <span
                      className="peity-pie"
                      data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}'
                      data-width="54"
                      data-height="54"
                    >
                      3/8
                    </span>
                  </div>
                </div>
                <div className="mx-3">
                  <div className="card mb-0 border px-3 py-1 pt-3 mini-stats-desc">
                    <div className="d-flex">
                      <h6 className="mt-0 mb-0 leading-none">Users</h6>
                      <h5 className="ms-auto mt-0 leading-none">$17.5</h5>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="card mini-stats">
                <div className="p-3 mini-stats-content">
                  <div className="mb-4">
                    <div className="float-end text-end">
                      <span className="badge bg-light text-info my-2">
                        {" "}
                        - 89%{" "}
                      </span>
                      <p className="text-white-50">Total Revenue</p>
                    </div>
                    <span
                      className="peity-donut"
                      data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"], "innerRadius": 18, "radius": 32 }'
                      data-width="54"
                      data-height="54"
                    >
                      3/5
                    </span>
                  </div>
                </div>
                <div className="mx-3">
                  <div className="card mb-0 border px-3 py-1 pt-3 mini-stats-desc">
                    <div className="d-flex">
                      <h6 className="mt-0 mb-0 leading-none">Revenue</h6>
                      <h5 className="ms-auto mt-0 leading-none">2048</h5>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="row">
            <div className="col-xl-9">
              <div className="card">
                <div className="card-body">
                  <h1 className="mt-0 header-title mb-5">Monthly Earning</h1>
                  <div
                    id="morris-bar-stacked"
                    className="morris-chart-height morris-charts"
                    data-colors='["#1699dd", "--bs-primary", "#ebeff2"]'
                  ></div>
                </div>
              </div>
            </div>

            <div className="col-xl-3">
              <div className="card">
                <div className="card-body">
                  <h4 className="mt-0 header-title">Sales Analytics</h4>

                  <div className="row text-center mt-5">
                    <div className="col-6">
                      <h5 className="">56241</h5>
                      <p className="text-muted font-size-14">Marketplace</p>
                    </div>
                    <div className="col-6">
                      <h5 className="">23651</h5>
                      <p className="text-muted font-size-14">Total Income</p>
                    </div>
                  </div>

                  <div
                    id="morris-donut-example"
                    className="dash-chart morris-charts text-center"
                    data-colors='["#ebeff2", "--bs-primary", "#1699dd"]'
                  ></div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-4 col-md-6">
              <div className="card">
                <div className="card-body">
                  <h4 className="mt-0 header-title">Weekly Sales</h4>
                  <div className="py-4">
                    <span
                      className="peity-line"
                      data-width="100%"
                      data-peity='{ "fill": ["rgba(22, 153, 221, 0.3)"],"stroke": ["rgba(22, 153, 221, 0.9)"]}'
                      data-height="60"
                    >
                      4,6,8,7,6,7,9,8,6,5,7,5,8,6,4,8,9,8,5,4
                    </span>
                  </div>

                  <div className="row">
                    <div className="col-6">
                      <div className="weekly-sale-list text-center">
                        <h5>145</h5>
                        <p className="text-muted mb-0">This Week</p>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="weekly-sale-list text-center">
                        <h5>132</h5>
                        <p className="text-muted mb-0">Last Week</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card product-sales">
                <div className="card-body">
                  <h5 className="mt-0 mb-4">
                    <i className="ion-monitor h4 me-2 text-primary"></i>{" "}
                    Computers
                  </h5>
                  <div className="row align-items-center mb-4">
                    <div className="col-6">
                      <p className="text-muted">This Month Sales</p>
                      <h4>
                        <sup className="me-1">
                          <small>$</small>
                        </sup>
                        14,352
                      </h4>
                    </div>
                    <div className="col-6">
                      <div className="text-center">
                        <span
                          className="peity-pie"
                          data-peity='{ "fill": ["rgba(22, 153, 221, 1)", "#f2f2f2"]}'
                          data-width="65"
                          data-height="65"
                        >
                          70/100
                        </span>
                      </div>
                    </div>
                  </div>
                  <div>
                    <p className="text-muted mb-3">Top Cities Sales</p>
                    <div className="row">
                      <div className="col-6">
                        <p className="text-muted mb-2">
                          Los Angeles : <b className="text-dark">$ 8,235</b>
                        </p>
                      </div>
                      <div className="col-6">
                        <p className="text-muted mb-2">
                          San Francisco : <b className="text-dark">$ 7,256</b>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4 col-md-6">
              <div className="card messages">
                <div className="card-body">
                  <h4 className="mt-0 header-title">Latest Messages</h4>

                  <nav className="mt-4">
                    <div
                      className="nav nav-tabs latest-messages-tabs nav-justified"
                      id="nav-tab"
                      role="tablist"
                    >
                      <a
                        className="nav-item nav-link active"
                        id="nav-first-tab"
                        data-bs-toggle="tab"
                        href="#nav-first"
                        role="tab"
                        aria-controls="nav-first"
                        aria-selected="true"
                      >
                        <h4 className="mt-0">12</h4>
                        <p className="text-muted mb-0">November</p>
                      </a>
                      <a
                        className="nav-item nav-link"
                        id="nav-second-tab"
                        data-bs-toggle="tab"
                        href="#nav-second"
                        role="tab"
                        aria-controls="nav-second"
                        aria-selected="false"
                      >
                        <h4 className="mt-0">13</h4>
                        <p className="text-muted mb-0">November</p>
                      </a>
                    </div>
                  </nav>
                  <div className="tab-content" id="nav-tabContent">
                    <div
                      className="tab-pane show active"
                      id="nav-first"
                      role="tabpanel"
                      aria-labelledby="nav-first-tab"
                    >
                      <div className="p-2 mt-2">
                        <ul className="list-unstyled latest-message-list mb-0">
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-2.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Michael Bowen</h6>
                                  <p className="text-muted mb-0">
                                    Hey! there Im available...
                                  </p>
                                  <p className="time text-muted">Just Now</p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-3.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Danny Benson</h6>
                                  <p className="text-muted mb-0">
                                    Ive finished it! See you so...
                                  </p>
                                  <p className="time text-muted">12 min ago</p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-4.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Brady Smith</h6>
                                  <p className="text-muted mb-0">
                                    This theme is awesome!
                                  </p>
                                  <p className="time text-muted">23 min ago</p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-5.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Danny Benson</h6>
                                  <p className="text-muted mb-0">
                                    Nice to meet you
                                  </p>
                                  <p className="time text-muted">34 min ago</p>
                                </div>
                              </div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div
                      className="tab-pane"
                      id="nav-second"
                      role="tabpanel"
                      aria-labelledby="nav-second-tab"
                    >
                      <div className="p-2 mt-2">
                        <ul className="list-unstyled latest-message-list mb-0">
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-5.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Albert Jones</h6>
                                  <p className="text-muted mb-0">
                                    Hey! there Im available...
                                  </p>
                                  <p className="time text-muted">
                                    yesterday, 09:42am
                                  </p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-6.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Danny Benson</h6>
                                  <p className="text-muted mb-0">
                                    Ive finished it! See you so...
                                  </p>
                                  <p className="time text-muted">
                                    yesterday, 11:07am
                                  </p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-7.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Joseph Sanchez</h6>
                                  <p className="text-muted mb-0">
                                    This theme is awesome!
                                  </p>
                                  <p className="time text-muted">
                                    yesterday, 01:17am
                                  </p>
                                </div>
                              </div>
                            </a>
                          </li>
                          <li className="message-list-item">
                            <a href="#" className="text-dark">
                              <div className="d-flex">
                                <img
                                  className="me-3 avatar-sm rounded-circle"
                                  src="assets/images/users/avatar-5.jpg"
                                  alt=""
                                />
                                <div className="">
                                  <h6 className="mt-0">Daniel Anderson</h6>
                                  <p className="text-muted mb-0">
                                    Nice to meet you
                                  </p>
                                  <p className="time text-muted">34 min ago</p>
                                </div>
                              </div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4">
              <div className="card">
                <div className="card-body">
                  <h4 className="mt-0 header-title">Recent activity</h4>

                  <div className="p-2">
                    <ul className="list-unstyled rec-acti-list">
                      <li className="rec-acti-list-item">
                        <div>
                          <p className="text-muted mb-1">14 Oct, 09:30am</p>
                          <h6 className="mb-0">
                            <a href="#" className="text-dark">
                              Meeting For a new Projects
                            </a>
                          </h6>
                          <div className="delete-icon">
                            <a href="#" className="text-primary">
                              <i className="mdi mdi-delete-forever h4"></i>
                            </a>
                          </div>
                        </div>
                      </li>
                      <li className="rec-acti-list-item">
                        <div>
                          <p className="text-muted mb-1">14 Oct, 11:05am</p>
                          <h6 className="mb-0">
                            <a href="#" className="text-dark">
                              Start a new Projects
                            </a>
                          </h6>
                          <div className="delete-icon">
                            <a href="#" className="text-primary">
                              <i className="mdi mdi-delete-forever h4"></i>
                            </a>
                          </div>
                        </div>
                      </li>
                      <li className="rec-acti-list-item">
                        <div>
                          <p className="text-muted mb-1">15 Oct, 10:16am</p>
                          <h6 className="mb-0">
                            <a href="#" className="text-dark">
                              Create Landing Psd template
                            </a>
                          </h6>
                          <div className="delete-icon">
                            <a href="#" className="text-primary">
                              <i className="mdi mdi-delete-forever h4"></i>
                            </a>
                          </div>
                        </div>
                      </li>
                      <li className="rec-acti-list-item">
                        <div>
                          <p className="text-muted mb-1">16 Oct, 11:24am</p>
                          <h6 className="mb-0">
                            <a href="#" className="text-dark">
                              Convert Psd to Html
                            </a>
                          </h6>
                          <div className="delete-icon">
                            <a href="#" className="text-primary">
                              <i className="mdi mdi-delete-forever h4"></i>
                            </a>
                          </div>
                        </div>
                      </li>
                      <li className="rec-acti-list-item">
                        <div>
                          <p className="text-muted mb-1">17 Oct, 01:36pm</p>
                          <h6 className="mb-0">
                            <a href="#" className="text-dark">
                              Redesign Html template
                            </a>
                          </h6>
                          <div className="delete-icon">
                            <a href="#" className="text-primary">
                              <i className="mdi mdi-delete-forever h4"></i>
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <form>
                    <div className="row">
                      <div className="col-8 rec-acti-input">
                        <input
                          type="text"
                          name="rec-input-text"
                          className="form-control"
                          placeholder="Add new Activity"
                        />
                      </div>
                      <div className="col-4 rec-acti-send">
                        <div className="d-grid">
                          <button className="btn-primary btn" type="button">
                            <i className="mdi mdi-plus me-1"></i>Add
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h4 className="mt-0 header-title">Latest Transactions</h4>
                  <div className="table-responsive mt-4">
                    <table className="table table-hover mb-0">
                      <thead>
                        <tr>
                          <th scope="col">(#) Id</th>
                          <th scope="col">Name</th>
                          <th scope="col">Date</th>
                          <th scope="col">Status</th>
                          <th scope="col">Price</th>
                          <th scope="col">Quantity</th>
                          <th scope="col" colSpan="2">
                            Amount
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">#16252</th>
                          <td>
                            <div>
                              <img
                                src="assets/images/users/avatar-2.jpg"
                                alt=""
                                className="avatar-xs rounded-circle me-2"
                              />{" "}
                              Rafael Reardon
                            </div>
                          </td>
                          <td>14/10/2018</td>
                          <td>
                            <span className="badge bg-success">Delivered</span>
                          </td>
                          <td>$80</td>
                          <td>1</td>
                          <td>$80</td>
                          <td>
                            <div>
                              <a href="#" className="btn btn-primary btn-sm">
                                Edit
                              </a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">#16253</th>
                          <td>
                            <div>
                              <img
                                src="assets/images/users/avatar-3.jpg"
                                alt=""
                                className="avatar-xs rounded-circle me-2"
                              />{" "}
                              Thomas Hirsch
                            </div>
                          </td>
                          <td>15/10/2018</td>
                          <td>
                            <span className="badge bg-warning">Pending</span>
                          </td>
                          <td>$76</td>
                          <td>2</td>
                          <td>$152</td>
                          <td>
                            <div>
                              <a href="#" className="btn btn-primary btn-sm">
                                Edit
                              </a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">#16254</th>
                          <td>
                            <div>
                              <img
                                src="assets/images/users/avatar-4.jpg"
                                alt=""
                                className="avatar-xs rounded-circle me-2"
                              />{" "}
                              Archer Desaillly
                            </div>
                          </td>
                          <td>15/10/2018</td>
                          <td>
                            <span className="badge bg-success">Delivered</span>
                          </td>
                          <td>$86</td>
                          <td>1</td>
                          <td>$86</td>
                          <td>
                            <div>
                              <a href="#" className="btn btn-primary btn-sm">
                                Edit
                              </a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">#16255</th>
                          <td>
                            <div>
                              <img
                                src="assets/images/users/avatar-5.jpg"
                                alt=""
                                className="avatar-xs rounded-circle me-2"
                              />{" "}
                              Michael Flannery
                            </div>
                          </td>
                          <td>16/10/2018</td>
                          <td>
                            <span className="badge bg-danger">Cancel</span>
                          </td>
                          <td>$82</td>
                          <td>2</td>
                          <td>$164</td>
                          <td>
                            <div>
                              <a href="#" className="btn btn-primary btn-sm">
                                Edit
                              </a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">#16256</th>
                          <td>
                            <div>
                              <img
                                src="assets/images/users/avatar-6.jpg"
                                alt=""
                                className="avatar-xs rounded-circle me-2"
                              />{" "}
                              Jamie Fishbourne
                            </div>
                          </td>
                          <td>17/10/2018</td>
                          <td>
                            <span className="badge bg-success">Delivered</span>
                          </td>
                          <td>$84</td>
                          <td>2</td>
                          <td>$84</td>
                          <td>
                            <div>
                              <a href="#" className="btn btn-primary btn-sm">
                                Edit
                              </a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </>
  );
}

export default Home;
