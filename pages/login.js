import React from "react";

function Login(props) {
  return (
    <>
      <div class="account-pages gradient_bg flex justify-center items-center h-screen">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-8 col-lg-5 col-xl-4">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex p-3">
                    <div>
                      <a href="index.html" class="">
                        <img
                          src="assets/images/logo_dark.png"
                          alt=""
                          height="22"
                          class="auth-logo logo-dark"
                        />
                        <img
                          src="assets/images/logo.png"
                          alt=""
                          height="22"
                          class="auth-logo logo-light"
                        />
                      </a>
                    </div>
                    <div class="ms-auto text-end">
                      <h4 class="font-size-18">Welcome Back !</h4>
                      <p class="text-muted mb-0">
                        Sign in to continue to seemyAi.
                      </p>
                    </div>
                  </div>
                  <div class="p-3">
                    <form
                      class="form-horizontal"
                      action="https://Shahzad-Ali.com/seemyAi/layouts/index.html"
                    >
                      <div class="mb-3">
                        <label class="form-label" for="username">
                          Username
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          id="username"
                          placeholder="Enter username"
                        />
                      </div>

                      <div class="mb-3">
                        <label class="form-label" for="userpassword">
                          Password
                        </label>
                        <input
                          type="password"
                          class="form-control"
                          id="userpassword"
                          placeholder="Enter password"
                        />
                      </div>

                      <div class="row mt-4">
                        <div class="col-sm-6">
                          <div class="form-check">
                            <input
                              class="form-check-input"
                              type="checkbox"
                              value=""
                              id="customControlInline"
                            />
                            <label
                              class="form-check-label"
                              for="customControlInline"
                            >
                              Remember me
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-6 text-end">
                          <button
                            class="btn btn-primary w-md waves-effect waves-light"
                            type="submit"
                          >
                            Log In
                          </button>
                        </div>
                      </div>

                      <div class="mb-0 row">
                        <div class="col-12 mt-4 text-center">
                          <a href="pages-recoverpw.html" class="text-muted">
                            <i class="mdi mdi-lock"></i> Forgot your password?
                          </a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="mt-5 text-center position-relative">
                <p class="text-white-50">
                  Dont have an account ?{" "}
                  <a href="pages-register.html" class="fw-bold text-white">
                    {" "}
                    Signup Now{" "}
                  </a>{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}


export default Login;
