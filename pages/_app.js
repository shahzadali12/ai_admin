import '@/styles/globals.css'
// import "../public/css/app-dark.min.css"
// import "../public/css/app-rtl.min.css"
import "../public/css/bootstrap.min.css"
import "../public/css/icons.min.css"
import "../public/css/app.min.css"
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import DefaultLayout from '@/layouts/default'
export default function App({ Component, pageProps }) {
  return <DefaultLayout><Component {...pageProps} /></DefaultLayout>
}
