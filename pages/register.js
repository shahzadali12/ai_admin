import React from "react";

function Login(props) {
  return (
    <>
      <div class="account-pages gradient_bg flex justify-center items-center h-screen">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-8 col-lg-5 col-xl-5">
              <div class="card mx-5">
                <div class="card-body">
                  <div class="d-flex p-3">
                    <div>
                      <a href="index.html" class="">
                        <img
                          src="/images/logo_dark.png"
                          alt=""
                          height="22"
                          class="auth-logo logo-dark"
                        />
                        <img
                          src="/images/logo.png"
                          alt=""
                          height="22"
                          class="auth-logo logo-light"
                        />
                      </a>
                    </div>
                    <div class="ms-auto text-end">
                      <h4 class="font-size-18">Free Register</h4>
                      <p class="text-muted mb-0">
                        Get your free seemyAi account now.
                      </p>
                    </div>
                  </div>
                  <div class="p-3">
                    <form
                      class="form-horizontal"
                      action="https://themesbrand.com/seemyAi/layouts/index.html"
                    >
                      <div class="mb-3">
                        <label class="form-label" for="useremail">
                          Email
                        </label>
                        <input
                          type="email"
                          class="form-control"
                          id="useremail"
                          placeholder="Enter email"
                        />
                      </div>

                      <div class="mb-3">
                        <label class="form-label" for="username">
                          Username
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          id="username"
                          placeholder="Enter username"
                        />
                      </div>

                      <div class="mb-3">
                        <label class="form-label" for="userpassword">
                          Password
                        </label>
                        <input
                          type="password"
                          class="form-control"
                          id="userpassword"
                          placeholder="Enter password"
                        />
                      </div>

                      <div class="mb-3">
                        <div class="text-end">
                          <button
                            class="btn btn-primary w-md waves-effect waves-light"
                            type="submit"
                          >
                            Register
                          </button>
                        </div>
                      </div>

                      <div class="mb-0 row">
                        <div class="col-12 mt-4 text-center">
                          <p class=" text-muted mb-0">
                            By registering you agree to the seemyAi{" "}
                            <a href="#">Terms of Use</a>
                          </p>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="mt-5 text-center position-relative">
                <p class="text-white-50">
                  Already have an account ?{" "}
                  <a href="pages-login.html" class="fw-bold text-white">
                    Login
                  </a>{" "}
                </p>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
