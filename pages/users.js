import React, { useEffect, useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react";


import axios from "axios";
import { toast } from "react-toastify";

import Overlay from "../components/overlay";
import EditUser from "@/components/userFom";


function USERS() {
  const [showoverlay, setshowOverlay] = useState(false);
  const handleClose = () => setshowOverlay(false);

  const handleShow = () => {
    setshowOverlay(true);
    setRowdata([])
  }

  const [rawdata, setrawDate] = useState([]);

  const [Rowdata, setRowdata] = useState([]);

  const [Refresh, setRefresh] = useState(false);


  const columnDefs = [
    
    // {
    //   field: "userName",
    //   headerName: "userName",
    //   filter: "agTextColumnFilter",
    //   suppressMenu: true,
    //   minWidth: 300,
    //   maxWidth: 300,
    //   suppressSizeToFit: true,
    // },
    

    {
      field: "name",
      headerName: "name",
      filter: "agNumberColumnFilter",
      suppressMenu: true,
      minWidth: 300,
      maxWidth: 300,
      suppressSizeToFit: true,
    },
    {
      field: "email",
      headerName: "email",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      minWidth: 320,
      maxWidth: 320,
      suppressSizeToFit: true,
    },
    {
      field: "role",
      headerName: "role",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      minWidth: 300,
      maxWidth: 300,
      suppressSizeToFit: true,
    },
     
    
    {
      field: "Actions",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      minWidth: 250,
      maxWidth: 250,
      suppressSizeToFit: true,
      cellRenderer: (params) => HandleAction(params),
    },

  ];
  const HandleAction = (params) => {


    const Edit = () => {
      setshowOverlay(true)
      setRowdata(params.data)
    }
    const Remove = async () => {
      const { assisid, instructions,name,_id } = params.data
      const data = {
        assisid: assisid,
        instructions: instructions,
        name:name,
        _id:_id
      }
      await axios.delete(`${process.env.REACT_APP_BaseURL}/api/users`, { data }).then((val) => {
        toast.success("successfully Remove")
        setRefresh(val)
      });
    }
    return <>
      {/* <button onClick={Approved} className="px-2 py-1.5 bg-green-600 inline-flex rounded-md mr-2 text-white"><i className="fa fa-check"></i></button> */}
      {/* <button className="px-2 py-1.5 bg-red-400 inline-flex rounded-md mr-2 text-white"><i className="fa fa-ban"></i></button> */}
      <button className="px-2 py-1.5 bg-yellow-500 inline-flex rounded-md mr-2 text-white" onClick={Edit}><i className="fa fa-edit"></i></button>
      <button className="px-2 py-1.5 bg-red-500 inline-flex rounded-md mr-2 text-white" onClick={Remove}><i className="fa fa-times"></i></button>
    </>
  }

  const defaultColDef = useMemo(() => {
    return {
      editable: true,
      sortable: true,
      resizable: false,
      filter: true,
      floatingFilter: true,
      flex: 1,
      // width: 100,
      // minWidth: 100,
    };
  }, []);


  const listingData = async () => {
    const api = await fetch(`${process.env.REACT_APP_BaseURL}/api/users`)
    const res = await api.json();
    setrawDate(res)
  }

  useEffect(() => {
    listingData();
  }, [Refresh])
  return (
    <>
      <div className="p-4 grid grid-cols-12 mt-10">
        <div className="column col-span-12 lg:col-span-8 mb-2 mt-3">
          <h3>Manage Users</h3>
        </div>
        <div className="col-span-12 lg:col-span-4">
          <div className="text-lg-end mt-3">
            <button className="btn-normal bg-blue-500 px-3 py-2 rounded-md text-white" onClick={handleShow}>Add User</button>
          </div>
        </div>

        <Overlay showoverlay={showoverlay} handleClose={handleClose} data={<EditUser Rowdata={Rowdata} setshowOverlay={setshowOverlay} setRefresh={setRefresh} />} />
        <div className="col-span-12 h-screen">
          <AgGridReact
            className="ag-theme-alpine h-screen"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            rowData={rawdata}
            alwaysShowHorizontalScroll={true}
            alwaysShowVerticalScroll={true}
            animateRows={true}
          />
        </div>
      </div>
    </>

  )
}

export default USERS;