/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  env:{
    REACT_APP_BaseURL: process.env.NODE_ENV === "development" ? "http://localhost:3000" : "https://seemyai.com"
  }
}

module.exports = nextConfig
