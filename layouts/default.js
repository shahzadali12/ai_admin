import Header from "@/components/header";
import SideBar from "@/components/sidebar";
import { useRouter } from "next/router";
import React from "react";

function DefaultLayout({children}) {
    const route = useRouter();

  return (
    <>
      <div className="wrapperD">
        {route.pathname != "/login" && route.pathname != "/register" ? <><Header/>
        <SideBar/>
        <div className="main-content">
          {children}
        </div></> : <>{children}</>}
      </div>
    </>
  );
}

export default DefaultLayout;
